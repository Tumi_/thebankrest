# theBankRest

PREREQUISITES:

The following items should be installed in your system:

Java 8 or newer.
git command line tool (https://help.github.com/articles/set-up-git)
Your prefered IDE
Eclipse with the m2e plugin. Note: when m2e is available, there is an m2 icon in Help -> About dialog. If m2e is not there, just follow the install process here: https://www.eclipse.org/m2e/
Spring Tools Suite (STS)
IntelliJ IDEA

STEPS:

On the command line

Clone the repo

Inside Eclipse or STS
File -> Import -> Maven -> Existing Maven project
Then either build on the command line ./mvnw generate-resources or using the Eclipse launcher (right click on project and Run As -> Maven install) to generate the css. Run the application main method by right clicking on it and choosing Run As -> Java Application.

Inside IntelliJ IDEA
In the main menu, choose File -> Open and select the Petclinic pom.xml. Click on the Open button.

CSS files are generated from the Maven build. You can either build them on the command line ./mvnw generate-resources or right click on the spring-petclinic project then Maven -> Generates sources and Update Folders.

A run configuration named TheBankRestApplication should have been created for you if you're using a recent Ultimate version. Otherwise, run the application by right clicking on the TheBankRestApplication main class and choosing Run 'TheBankRestApplication'.

Navigate to TheBankRest
Visit http://localhost:8080 in your browser.

THE DATABASE (SCHEMA AND DATA) is found in the resources folder, navigate and run 'thebankd_db' on your local machine



