package com.eoh.thebankrest.service;

import com.eoh.thebankrest.entity.Account;
import com.eoh.thebankrest.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    @Autowired
    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository){
       this.accountRepository = accountRepository;
    }

    public List<Account> getAllAUserAccounts(Long user_id){
        return accountRepository.getAllUserAccounts(user_id);
    }

    public Account getById(Long id){
        return accountRepository.getOne(id);
    }

    public Account update(Long id, Account account){

        //Searching if account exists
        Account account1 = getById(id);
        double newbal;

        if (account1.getBalance() < 100){
            return null;
        } else {
            newbal = account1.getBalance() - account.getBalance();

            account1.setBalance(newbal);
            return accountRepository.save(account1);
        }
    }

    public Double getHighestBalAcc(Long id){
        return accountRepository.getHighestAccBal(id);
    }
}
