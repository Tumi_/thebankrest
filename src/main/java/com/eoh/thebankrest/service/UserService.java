package com.eoh.thebankrest.service;

import com.eoh.thebankrest.entity.User;
import com.eoh.thebankrest.repository.AccountRepository;
import com.eoh.thebankrest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User login(String password) {
        User user = userRepository.getUserByPassword(password);
//        if (user == null){
//            throw new NotFoundException("Account not found");
//        } else {
//            return user;
//        }
        return user;
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User getUser(String surname){
        return  userRepository.getUserBySurname(surname);
    }
}
