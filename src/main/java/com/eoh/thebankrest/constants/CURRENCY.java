package com.eoh.thebankrest.constants;

public enum CURRENCY {
    USD, EUR, CAD
}
