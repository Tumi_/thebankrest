package com.eoh.thebankrest.controller;

import com.eoh.thebankrest.entity.Account;
import com.eoh.thebankrest.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user/accounts")
@RestController
@CrossOrigin("*")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/all/{id}")
    public List<Account> getUserAccounts(@PathVariable long id){
        return accountService.getAllAUserAccounts(id);
    }

    @GetMapping("/{id}")
    public Account getAccount(@PathVariable long id){
        return accountService.getById(id);
    }

    @PutMapping("withdraw/{id}")
    public ResponseEntity<Account> withdraw(@PathVariable long id, @RequestBody Account account){
        //I will Return a response entity instead
        Account account1 = accountService.update(id, account);

        if (account1 != null){
            return new ResponseEntity<Account>(account1, HttpStatus.OK);
        } else {
//            return ResponseEntity.noContent().build();
            throw new IllegalArgumentException("Error Withdrawing");
        }
    }

    @GetMapping("highest/{id}")
    public Double getHighestBal(@PathVariable long id){
        return accountService.getHighestBalAcc(id);
    }
//    @GetMapping("/sendmail")
}
