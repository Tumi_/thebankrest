package com.eoh.thebankrest.controller;

import com.eoh.thebankrest.entity.Account;
import com.eoh.thebankrest.entity.User;
import com.eoh.thebankrest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin("*")
public class UserController {


    private final UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping("/login")
    public User login(@RequestBody User user){
        String password = user.getPassword();
        return userService.login(password);
    }

    @GetMapping("/getUsers")
    public List<User> getAll(){
        return userService.getAllUsers();
    }

    @GetMapping("/getUser/{surname}")
    public User getUserBySurname(@PathVariable String surname){
        return userService.getUser(surname);
    }


}
