package com.eoh.thebankrest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;

@RestController
public class EmailController {

    @Autowired
    private JavaMailSender sender;

    @RequestMapping("/sendemail")
    public ResponseEntity<Void> sendEmail() throws Exception{
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo("itumelengkatali@gmail.com");
        helper.setText("How are you?");
        helper.setSubject("Hi");

        sender.send(message);
        return ResponseEntity.noContent().build();
    }


}
