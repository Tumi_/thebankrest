package com.eoh.thebankrest.repository;

import com.eoh.thebankrest.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

//    @Query("SELECT u from User u Where u.password=?1")
//    User login(String password);
    User getUserByPassword(String password);

    User getUserBySurname(String surname);

}
