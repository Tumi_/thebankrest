package com.eoh.thebankrest.repository;

import com.eoh.thebankrest.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("select a from Account a WHERE user.id = ?1")
    List<Account> getAllUserAccounts(Long id);

    @Query("select MAX(a.balance) from Account a WHERE user.id = ?1")
            Double getHighestAccBal(Long id);
//    Account getHighestBal(Long id);


    Account getAccountByBalance(Double balance);
}
